from jqdatasdk import *
import configparser

local_mod = "local"

config = configparser.ConfigParser()
config.read("jq.conf", encoding="utf-8")
mod = config.get('jq', 'mod')
print("start with mod " + mod)
if mod != local_mod:
    auth(config.get('jq', 'id'), config.get('jq', 'passwd'))
cache_root_path = config.get('jq', 'cacheRootPath')
