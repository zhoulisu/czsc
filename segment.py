from typing import List

from czsc.abstractTrend import AbstractTrendListener, SimpleTrendImpl
from czsc.base import DirectType
from czsc.base import PointType
from czsc.drawing import Drawing
from czsc.drawing import DrawingEventListener
from czsc.point import CzscPoint
from czsc.quote import QuoteLevel

AT_LEAST_SEGMENT_DRAWING_NUM = 4


class Segment(SimpleTrendImpl):
    """
    缠论段
    缠论由笔构成段，段作为最低级别行情的次级别走势类型，构成最低级别中枢
    因此理论上，段的行为应与走势类型一致
    """

    def __init__(self, start: CzscPoint, end: CzscPoint, level: QuoteLevel):
        super().__init__(start, end, level)


def is_segment_start(point: CzscPoint, segment_points: List[CzscPoint], direct: DirectType) -> bool:
    func = min if direct is DirectType.UP else max
    return point.value() is func([point.value() for point in segment_points])


def is_segment_end(point: CzscPoint, segment_points: List[CzscPoint], direct: DirectType) -> bool:
    func = max if direct is DirectType.UP else min
    return point.value() is func([point.value() for point in segment_points])


def extreme_func(direct: DirectType):
    return max if direct is DirectType.UP else min


def extreme_point(points: List[CzscPoint], direct: DirectType) -> CzscPoint:
    values = [p.value() for p in points]
    return [p for p in points if p.value() is extreme_func(direct)(values)][0]


class SegmentBuilder(DrawingEventListener):

    def __init__(self, level: QuoteLevel, listeners: List[AbstractTrendListener] = [], immedite: bool = False):
        self.level = level
        self.segments: List[CzscPoint] = []
        self.already_segment_points: List[CzscPoint] = []
        self.uncertain_segment_points: List[CzscPoint] = []
        self.already_segment_direct: DirectType = None
        self.already_segment_2nd_extreme: CzscPoint = None
        self.listeners: List[AbstractTrendListener] = listeners
        self.received_drawing: Drawing = None
        self.last_status: SegmentBuilder = None
        self.immedite: bool = immedite;
        self.is_last_immedite: bool = False

    def save_status(self):
        last_status = SegmentBuilder(self.level, immedite=self.immedite)
        last_status.segments = self.segments[:]
        last_status.already_segment_points = self.already_segment_points[:]
        last_status.uncertain_segment_points = self.uncertain_segment_points[:]
        last_status.already_segment_direct = self.already_segment_direct
        last_status.already_segment_2nd_extreme = self.already_segment_2nd_extreme
        last_status.received_drawing = self.received_drawing
        last_status.is_last_immedite = self.is_last_immedite
        self.last_status = last_status

    def rollback(self):
        last_status: SegmentBuilder = self.last_status
        self.segments = last_status.segments
        self.already_segment_points = last_status.already_segment_points
        self.uncertain_segment_points = last_status.uncertain_segment_points
        self.already_segment_direct = last_status.already_segment_direct
        self.already_segment_2nd_extreme = last_status.already_segment_2nd_extreme
        self.received_drawing = last_status.received_drawing
        self.is_last_immedite = last_status.is_last_immedite
        self.last_status = None

    def receive_drawing(self, drawing: Drawing):
        if self.last_status is not None and drawing.same(self.last_status.received_drawing):
            # 如果新接收的drawing为上一次drawing的更新，回退到上次接收处理前的状态
            self.rollback()
        self.received_drawing = drawing
        self.save_status()
        point = drawing.end
        self.uncertain_segment_points.append(point)
        # 尚无已成立段结构形成
        if len(self.already_segment_points) == 0:
            uncertain_length = len(self.uncertain_segment_points)
            for i in range(uncertain_length - AT_LEAST_SEGMENT_DRAWING_NUM, -1, -2):
                assume_direct = DirectType.UP if point.point_type is PointType.TOP else DirectType.DOWN
                if is_segment_end(point, self.uncertain_segment_points[i:], assume_direct) and is_segment_start(
                        self.uncertain_segment_points[i], self.uncertain_segment_points[i:], assume_direct):
                    self.already_segment_points = self.uncertain_segment_points[i:]
                    self.already_segment_direct = assume_direct
                    self.already_segment_2nd_extreme = extreme_point(self.uncertain_segment_points[i:][:-1],
                                                                     assume_direct)
                    self.append_segment_point(self.uncertain_segment_points[i])
                    self.append_segment_point(point)
                    self.uncertain_segment_points = self.uncertain_segment_points[-1:]
                    break
        # 已存在段结构
        else:
            if point.point_type is self.already_segment_direct.associated_point_type() \
                    and self.already_segment_points[-1].continuous(point):
                self.already_segment_2nd_extreme = self.already_segment_points[-1]
                self.already_segment_points.extend(self.uncertain_segment_points)
                self.update_segment_point(point)
                self.uncertain_segment_points = self.uncertain_segment_points[-1:]
            # 破坏

            elif point.point_type is not self.already_segment_direct.associated_point_type():
                immedite = (not self.is_last_immedite) and self.immedite and self.already_segment_points[0].continuous(
                    point)
                if (len(self.uncertain_segment_points) >= AT_LEAST_SEGMENT_DRAWING_NUM - 1 \
                    and self.uncertain_segment_points[-3].continuous(point)
                        and self.already_segment_2nd_extreme.break_through(point)
                ) or immedite:
                    self.append_segment_point(point, immedite)
                    self.already_segment_points = self.uncertain_segment_points[:]
                    self.already_segment_2nd_extreme = extreme_point(self.uncertain_segment_points[:-1],
                                                                     self.already_segment_direct.reverse())
                    self.already_segment_direct = self.already_segment_direct.reverse()
                    self.uncertain_segment_points = self.uncertain_segment_points[-1:]

    def append_segment_point(self, point: CzscPoint, is_immedite: bool = False):
        self.segments.append(point)
        self.is_last_immedite = is_immedite
        if len(self.segments) >= 2:
            for listener in self.listeners:
                listener.receive_trend(SimpleTrendImpl(self.segments[-2], self.segments[-1], self.level))

    def update_segment_point(self, point: CzscPoint):
        self.segments[-1] = point
        if len(self.segments) >= 2:
            for listener in self.listeners:
                listener.receive_trend(SimpleTrendImpl(self.segments[-2], self.segments[-1], self.level))
