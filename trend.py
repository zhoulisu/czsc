from typing import List

from czsc.abstractTrend import AbstractTrend
from czsc.abstractTrend import AbstractTrendListener
from czsc.base import DirectType
from czsc.base import PointType
from czsc.point import CzscPoint
from czsc.point import extreme_point
from czsc.quote import QuoteLevel


def standard_three_trend_on_direct(points: List[CzscPoint]) -> (bool, DirectType):
    """
    判断标准上下上或下上下三段是否构成
    判断标准
    上：低点上涨，高点均可
    下：高点下跌，低点均可
    若构成，则可确定起点一定是高级别走势起点
    """
    if len(points) < 4:
        return False, None
    a0 = points[-4]
    a2 = points[-2]
    a3 = points[-1]
    if not a0.continuous(a2):
        return True, DirectType.UP if a3.point_type is PointType.TOP else DirectType.DOWN
    return False, None


class SameLevelTrendBuilder(AbstractTrendListener):
    """
    基于同级别分解的趋势构造器
    """

    def __init__(self, listeners: List[AbstractTrendListener]):
        self.listeners = listeners
        self.trends: List[CzscPoint] = []
        self.already_trend_points: List[CzscPoint] = []
        self.uncertain_trend_points: List[CzscPoint] = []
        self.already_trend_direct: DirectType = None

        self.received_trend: AbstractTrend = None
        self.last_status: SameLevelTrendBuilder = None

    def save_status(self):
        last_status = SameLevelTrendBuilder(self.listeners)
        # todo
        last_status.received_trend = self.received_trend
        self.last_status = last_status

    def rollback(self):
        last_status = self.last_status
        # todo
        self.received_trend = last_status.received_trend
        self.last_status = None
        return

    def receive_trend(self, trend: AbstractTrend):
        if self.last_status is not None and self.last_status.received_trend.same(trend):
            self.rollback()
        self.received_trend = trend
        self.save_status()
        point = trend.get_end()
        self.uncertain_trend_points.append(point)
        # 尚无已成立趋势结构形成
        if len(self.already_trend_points) == 0:
            standard_three_achieved, direct = standard_three_trend_on_direct(self.uncertain_trend_points[-4:])
            if not standard_three_achieved:
                return
            self.already_trend_points = self.uncertain_trend_points[-4:]
            self.already_trend_direct = direct
            self.append_trend(self.uncertain_trend_points[-1])
        else:
            uncertain_edge_num = len(self.uncertain_trend_points)
            if uncertain_edge_num == 2:
                a0 = self.already_trend_points[0]
                a1 = self.already_trend_points[1]
                a2 = self.already_trend_points[2]
                a3 = self.already_trend_points[3]
                a4 = self.uncertain_trend_points[0]
                a5 = self.uncertain_trend_points[1]
                if (not a1.break_through(a4)) and a3.continuous(a5):
                    self.already_trend_points = [a0, a3, a5, a5]
                    self.uncertain_trend_points.clear()
                    self.update_trend(a5)
            elif uncertain_edge_num == 3:
                a0 = self.already_trend_points[0]
                a1 = self.already_trend_points[1]
                a2 = self.already_trend_points[2]
                a3 = self.already_trend_points[3]
                a4 = self.uncertain_trend_points[0]
                a5 = self.uncertain_trend_points[1]
                a6 = self.uncertain_trend_points[3]
                if a3.break_through(a6):
                    self.already_trend_points = [a3, a4, a5, a6]
                    self.already_trend_direct = self.already_trend_direct.reverse()
                    self.uncertain_trend_points.clear()
                    self.append_trend(a6)
            elif uncertain_edge_num == 4:
                a0 = self.already_trend_points[0]
                a1 = self.already_trend_points[1]
                a2 = self.already_trend_points[2]
                a3 = self.already_trend_points[3]
                a4 = self.uncertain_trend_points[0]
                a5 = self.uncertain_trend_points[1]
                a6 = self.uncertain_trend_points[3]
                a7 = self.uncertain_trend_points[4]
                if a1.break_through(a4) and not a3.break_through(a6):
                    self.already_trend_points = [a0, a5, a6, a7]
                    self.uncertain_trend_points.clear()
                    self.update_trend(a7)
                elif not a1.break_through(a4) and a7 is extreme_point([a0, a1, a2, a3, a4, a5, a6, a7]):
                    self.already_trend_points = [a0, a3, a4, a5]
                    self.uncertain_trend_points = []

    def append_trend(self, point: CzscPoint):
        self.trends.append(point)
        # todo notify listeners

    def update_trend(self, point: CzscPoint):
        self.trendsp[-1] = point
        # todo notify listeners


def three_overlap(points: List[CzscPoint]) -> (bool, DirectType):
    """
    判断三段重叠
    :param points:
    :return:
    """
    if len(points) < 4:
        return False, None
    a0: CzscPoint = points[-4]
    a3: CzscPoint = points[-1]
    return a0.break_through(a3), a3.point_type.associate_direct()


class SameLevelBuiltTrend(AbstractTrend):
    """
    基于同级别分解构造的趋势
    保留构成本级趋势的下级趋势
    """

    def __init__(self, level: QuoteLevel, lower_level_trend_points: List[CzscPoint]):
        self.lower_level_trend_points = lower_level_trend_points
        self.direct = DirectType.UP if lower_level_trend_points[-1].value() > \
                                       lower_level_trend_points[0].value() else DirectType.DOWN
        self.level = level

    def get_level(self) -> QuoteLevel:
        return self.level

    def get_start(self) -> CzscPoint:
        return self.lower_level_trend_points[0]

    def get_end(self) -> CzscPoint:
        return self.lower_level_trend_points[-1]

    def get_direct(self) -> DirectType:
        return self.direct


class ThreeOverlapTrendBuilder(AbstractTrendListener):
    """
    基于同级别分解的趋势构造器, 三段低级别趋势重叠即升级
    """

    def __init__(self, level: QuoteLevel, listeners: List[AbstractTrendListener] = []):
        self.level = level
        self.listeners: Listp[AbstractTrendListener] = listeners
        self.trends: List[CzscPoint] = []
        self.already_trend_points: List[CzscPoint] = []
        self.uncertain_trend_points: List[CzscPoint] = []
        self.already_trend_direct: DirectType = None

        self.received_trend: AbstractTrend = None
        self.last_status: ThreeOverlapTrendBuilder = None

    def save_status(self):
        last_status = ThreeOverlapTrendBuilder(self.level, self.listeners)
        last_status.trends = self.trends[:]
        last_status.already_trend_points = self.already_trend_points[:]
        last_status.uncertain_trend_points = self.uncertain_trend_points[:]
        last_status.already_trend_direct = self.already_trend_direct
        last_status.received_trend = self.received_trend
        self.last_status = last_status

    def rollback(self):
        last_status = self.last_status
        self.trends = last_status.trends
        self.already_trend_points = last_status.already_trend_points
        self.uncertain_trend_points = last_status.uncertain_trend_points
        self.already_trend_direct = last_status.already_trend_direct
        self.received_trend = None
        self.last_status = None
        return

    def receive_trend(self, trend: AbstractTrend):
        if self.last_status is not None and self.last_status.received_trend.same(trend):
            self.rollback()
        self.received_trend = trend
        self.save_status()
        point = trend.get_end()
        self.uncertain_trend_points.append(point)
        # 尚无已成立趋势结构形成
        if len(self.already_trend_points) == 0:
            # 三段重叠即成立
            three_overlap_achieved, direct = three_overlap(self.uncertain_trend_points[-4:])
            if not three_overlap_achieved:
                return
            self.already_trend_points = self.uncertain_trend_points[-4:]
            self.already_trend_direct = direct
            self.append_trend(self.uncertain_trend_points[-4])
            self.append_trend(self.uncertain_trend_points[-1])
            self.uncertain_trend_points.clear()
        else:
            uncertain_edge_num = len(self.uncertain_trend_points)
            # 再次三段重叠则新趋势成立
            if uncertain_edge_num == 3:
                points = [self.already_trend_points[-1]]
                points.extend(self.uncertain_trend_points)
                three_overlap_achieved, direct = three_overlap(points)
                if three_overlap_achieved:
                    self.already_trend_points = points
                    self.already_trend_direct = direct
                    self.append_trend(points[-1])
                    self.uncertain_trend_points.clear()
            # uncertain已到达4边时必定前3段未重叠，把前2点归属到already
            if uncertain_edge_num == 4:
                self.already_trend_points.append(self.uncertain_trend_points[-4])
                self.already_trend_points.append(self.uncertain_trend_points[-3])
                self.update_trend(self.uncertain_trend_points[-3])
                self.uncertain_trend_points = self.uncertain_trend_points[-2:]

    def append_trend(self, point: CzscPoint):
        self.trends.append(point)
        if len(self.trends) > 1:
            for listener in self.listeners:
                listener.receive_trend(SameLevelBuiltTrend(self.level, self.already_trend_points[:]))

    def update_trend(self, point: CzscPoint):
        self.trends[-1] = point
        if len(self.trends) > 1:
            for listener in self.listeners:
                listener.receive_trend(SameLevelBuiltTrend(self.level, self.already_trend_points[:]))


class ImmediateThreeOverlapTrendBuilder(AbstractTrendListener):
    """
    基于同级别分解的趋势构造器, 三段低级别趋势重叠即升级
    一旦出现新高或第即更新走势，允许只有一段低级别走势构成
    """

    def __init__(self, level: QuoteLevel, listeners: List[AbstractTrendListener]):
        self.level = level
        self.listeners: Listp[AbstractTrendListener] = listeners
        self.trends: List[CzscPoint] = []
        self.already_trend_points: List[CzscPoint] = []
        self.uncertain_trend_points: List[CzscPoint] = []
        self.already_trend_direct: DirectType = None

        self.received_trend: AbstractTrend = None
        self.last_status: ImmediateThreeOverlapTrendBuilder = None

    def save_status(self):
        last_status = ImmediateThreeOverlapTrendBuilder(self.level, self.listeners)
        last_status.trends = self.trends[:]
        last_status.already_trend_points = self.already_trend_points[:]
        last_status.uncertain_trend_points = self.uncertain_trend_points[:]
        last_status.already_trend_direct = self.already_trend_direct
        last_status.received_trend = self.received_trend
        self.last_status = last_status

    def rollback(self):
        last_status = self.last_status
        self.trends = last_status.trends
        self.already_trend_points = last_status.already_trend_points
        self.uncertain_trend_points = last_status.uncertain_trend_points
        self.already_trend_direct = last_status.already_trend_direct
        self.received_trend = None
        self.last_status = None
        return

    def receive_trend(self, trend: AbstractTrend):
        if self.last_status is not None and self.last_status.received_trend.same(trend):
            self.rollback()
        self.received_trend = trend
        self.save_status()
        point = trend.get_end()
        self.uncertain_trend_points.append(point)
        # 尚无已成立趋势结构形成
        if len(self.already_trend_points) == 0:
            # 标准三段重叠即成立
            three_overlap_achieved, direct = standard_three_trend_on_direct(self.uncertain_trend_points[-4:])
            if not three_overlap_achieved:
                return
            self.already_trend_points = self.uncertain_trend_points[-4:]
            self.already_trend_direct = direct
            self.append_trend(self.uncertain_trend_points[-4])
            self.append_trend(self.uncertain_trend_points[-1])
            self.uncertain_trend_points.clear()
        else:
            uncertain_edge_num = len(self.uncertain_trend_points)
            # 允许一段低级别构成的高级别走势
            if uncertain_edge_num == 1:
                if len(self.already_trend_points) > 4:
                    points = self.already_trend_points[-3:]
                    points.append(self.uncertain_trend_points[-1])
                    check, _ = three_overlap(points)
                    if check:
                        self.already_trend_points = [self.already_trend_points[-1], self.uncertain_trend_points[-1]]
                        self.append_trend(self.uncertain_trend_points[-1])
                        self.uncertain_trend_points.clear()
            # 两端突破前高/低则更新走势
            elif uncertain_edge_num == 2:
                if self.already_trend_points[-1].continuous(self.uncertain_trend_points[-1]):
                    self.already_trend_points.extend(self.uncertain_trend_points)
                    self.update_trend(self.uncertain_trend_points[-1])
                    self.uncertain_trend_points.clear()
            # 再次标准三段重叠则新趋势成立
            elif uncertain_edge_num == 3:
                points = [self.already_trend_points[-1]]
                points.extend(self.uncertain_trend_points)
                three_overlap_achieved, direct = standard_three_trend_on_direct(points)
                if three_overlap_achieved:
                    self.already_trend_points = points
                    self.already_trend_direct = direct
                    self.append_trend(points[-1])
                    self.uncertain_trend_points.clear()
            # uncertain已到达4边说明程序错误
            else:
                raise BaseException("invalid usage of ImmediateThreeOverlapTrendBuilder")

    def append_trend(self, point: CzscPoint):
        self.trends.append(point)
        if len(self.trends) > 1:
            for listener in self.listeners:
                listener.receive_trend(SameLevelBuiltTrend(self.level, self.already_trend_points[:]))

    def update_trend(self, point: CzscPoint):
        self.trends[-1] = point
        if len(self.trends) > 1:
            for listener in self.listeners:
                listener.receive_trend(SameLevelBuiltTrend(self.level, self.already_trend_points[:]))


class ImmediateTrendBuilder(AbstractTrendListener):
    """
    基于同级别分解的趋势构造器, 三段低级别趋势重叠即升级
    一旦出现新高或第即更新走势，允许只有一段低级别走势构成
    """

    def __init__(self, level: QuoteLevel, listeners: List[AbstractTrendListener], immedite: bool = False):
        self.level = level
        self.listeners: Listp[AbstractTrendListener] = listeners
        self.trends: List[CzscPoint] = []
        self.already_trend_points: List[CzscPoint] = []
        self.uncertain_trend_points: List[CzscPoint] = []
        self.already_trend_direct: DirectType = None
        self.received_trend: AbstractTrend = None
        self.last_status: ImmediateTrendBuilder = None
        self.immedite = immedite
        self.is_last_trend_immedite = False

    def save_status(self):
        last_status = ImmediateTrendBuilder(self.level, self.listeners)
        last_status.trends = self.trends[:]
        last_status.already_trend_points = self.already_trend_points[:]
        last_status.uncertain_trend_points = self.uncertain_trend_points[:]
        last_status.already_trend_direct = self.already_trend_direct
        last_status.received_trend = self.received_trend
        last_status.is_last_trend_immedite = self.is_last_trend_immedite
        self.last_status = last_status

    def rollback(self):
        last_status = self.last_status
        self.trends = last_status.trends
        self.already_trend_points = last_status.already_trend_points
        self.uncertain_trend_points = last_status.uncertain_trend_points
        self.already_trend_direct = last_status.already_trend_direct
        self.is_last_trend_immedite = last_status.is_last_trend_immedite
        self.received_trend = None
        self.last_status = None
        return

    def receive_trend(self, trend: AbstractTrend):
        if self.last_status is not None and self.last_status.received_trend.same(trend):
            self.rollback()
        self.received_trend = trend
        self.save_status()
        point = trend.get_end()
        self.uncertain_trend_points.append(point)
        # 尚无已成立趋势结构形成
        if len(self.already_trend_points) == 0:
            # 标准三段重叠即成立
            three_overlap_achieved, direct = standard_three_trend_on_direct(self.uncertain_trend_points[-4:])
            if not three_overlap_achieved:
                return
            self.already_trend_points = self.uncertain_trend_points[-4:]
            self.already_trend_direct = direct
            self.append_trend(self.uncertain_trend_points[-4])
            self.append_trend(self.uncertain_trend_points[-1])
            self.uncertain_trend_points.clear()
        else:
            uncertain_edge_num = len(self.uncertain_trend_points)
            # 允许一段低级别构成的高级别走势
            if uncertain_edge_num == 1:
                # 反向第一个低级别走势超过前走势时，认为反向走势成立
                if (not self.is_last_trend_immedite) and self.immedite \
                        and self.already_trend_points[0].continuous(self.uncertain_trend_points[-1]):
                    self.already_trend_points = [self.already_trend_points[-1], self.uncertain_trend_points[-1]]
                    self.append_trend(self.uncertain_trend_points[-1], is_immedite=True)
                    self.uncertain_trend_points.clear()
                if len(self.already_trend_points) > 4:
                    points = self.already_trend_points[-3:]
                    points.append(self.uncertain_trend_points[-1])
                    check, _ = three_overlap(points)
                    if check:
                        self.already_trend_points = [self.already_trend_points[-1], self.uncertain_trend_points[-1]]
                        self.append_trend(self.uncertain_trend_points[-1])
                        self.uncertain_trend_points.clear()
            # 两端突破前高/低则更新走势
            elif uncertain_edge_num == 2:
                if self.already_trend_points[-1].continuous(self.uncertain_trend_points[-1]):
                    self.already_trend_points.extend(self.uncertain_trend_points)
                    self.update_trend(self.uncertain_trend_points[-1])
                    self.uncertain_trend_points.clear()
            # 再次标准三段重叠则新趋势成立
            elif uncertain_edge_num == 3:
                points = [self.already_trend_points[-1]]
                points.extend(self.uncertain_trend_points)
                three_overlap_achieved, direct = standard_three_trend_on_direct(points)
                if three_overlap_achieved:
                    self.already_trend_points = points
                    self.already_trend_direct = direct
                    self.append_trend(points[-1])
                    self.uncertain_trend_points.clear()
            # uncertain已到达4边说明程序错误
            else:
                raise BaseException("invalid usage of ImmediateThreeOverlapTrendBuilder")

    def append_trend(self, point: CzscPoint, is_immedite=False):
        self.trends.append(point)
        self.is_last_trend_immedite = is_immedite
        if len(self.trends) > 1:
            for listener in self.listeners:
                listener.receive_trend(SameLevelBuiltTrend(self.level, self.already_trend_points[:]))

    def update_trend(self, point: CzscPoint):
        self.trends[-1] = point
        if len(self.trends) > 1:
            for listener in self.listeners:
                listener.receive_trend(SameLevelBuiltTrend(self.level, self.already_trend_points[:]))
