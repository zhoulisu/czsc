from abc import ABCMeta
from abc import abstractmethod
from typing import List

from czsc.abstractTrend import AbstractTrend
from czsc.abstractTrend import AbstractTrendListener
from czsc.base import DirectType
from czsc.point import CzscPoint
from czsc.point import PointType
from czsc.quote import QuoteLevel


class Maincenter(metaclass=ABCMeta):

    @abstractmethod
    def get_start(self) -> CzscPoint:
        pass

    @abstractmethod
    def get_end(self) -> CzscPoint:
        pass

    @abstractmethod
    def get_top(self) -> float:
        pass

    @abstractmethod
    def get_bottom(self) -> float:
        pass

    @abstractmethod
    def same(self, maincenter) -> bool:
        pass


class MaincenterListener(metaclass=ABCMeta):

    @abstractmethod
    def receive_maincenter(self, maincenter: Maincenter):
        pass


class SameLevelMainCenter(Maincenter):
    """
    同级别分解下中枢，三段以上重叠即为中枢
    主要用于脱离中枢后的第三交易点判断脱离本级别震荡
    """

    def __init__(self, points: List[CzscPoint]):
        self.points: List[CzscPoint] = points
        self.__update_top()
        self.__update_bottom()

    def append(self, point: CzscPoint):
        self.points.append(point)
        if point.point_type is PointType.BOTTOM:
            self.__update_bottom()
        else:
            self.__update_top()

    def update_last_point(self, point: CzscPoint):
        self.points[-1] = point
        if point.point_type is PointType.BOTTOM:
            self.__update_bottom()
        else:
            self.__update_top()

    def __update_top(self):
        self.top = min([x.value() for x in self.points if x.point_type is PointType.TOP])

    def __update_bottom(self):
        self.bottom = max([x.value() for x in self.points if x.point_type is PointType.BOTTOM])

    def get_start(self) -> CzscPoint:
        return self.points[0]

    def get_end(self) -> CzscPoint:
        return self.points[-1]

    def get_top(self) -> float:
        return self.top

    def get_bottom(self) -> float:
        return self.bottom

    def same(self, maincenter: Maincenter) -> bool:
        if maincenter.__class__ is not SameLevelMainCenter:
            raise BaseException("invalid usage of SameLevelMainCenter")
        return self.get_start() == maincenter.get_start()


def three_overlap(points: List[CzscPoint]) -> (bool, DirectType):
    """
    判断三段重叠
    :param points:
    :return:
    """
    if len(points) < 4:
        return False, None
    a0: CzscPoint = points[-4]
    a3: CzscPoint = points[-1]
    return a0.break_through(a3), a3.point_type.associate_direct()


class SameLevelMainCenterBuilder(AbstractTrendListener):

    def __init__(self, level: QuoteLevel, listeners: List[MaincenterListener]):
        self.uncertain_points: List[CzscPoint] = []
        self.main_centers: List[SameLevelMainCenter] = []
        self.level = level
        self.last_received_trend: AbstractTrend = None
        self.last_status = None
        self.listeners = listeners

    def save_status(self):
        last_status = SameLevelMainCenterBuilder(self.level, self.listeners)
        last_status.uncertain_points = self.uncertain_points[:]
        last_status.main_centers = self.main_centers[:]
        last_status.last_received_trend = self.last_received_trend
        self.last_status = last_status

    def rollback(self):
        last_status = self.last_status
        self.uncertain_points = last_status.uncertain_points
        self.main_centers = last_status.main_centers
        self.last_received_trend = None
        self.last_status = None

    def receive_trend(self, trend: AbstractTrend):
        update = self.last_received_trend is not None and trend.same(self.last_received_trend)
        self.last_received_trend = trend
        point: CzscPoint = trend.get_end()
        if update:
            if len(self.uncertain_points) == 0:
                self.main_centers[-1].update_last_point(point)
                self.notify()
                return
            else:
                self.uncertain_points[-1] = point
        else:
            self.uncertain_points.append(point)
        if len(self.main_centers) == 0:
            check, _ = three_overlap(self.uncertain_points)
            if check:
                self.main_centers.append(SameLevelMainCenter(self.uncertain_points[-4:]))
                self.uncertain_points.clear()
                self.notify()
        else:
            cur_main_center = self.main_centers[-1]
            if len(self.uncertain_points) == 1:
                main_center_edge_value = cur_main_center.top if point.point_type is PointType.BOTTOM else cur_main_center.bottom
                if point.over(main_center_edge_value):
                    cur_main_center.append(point)
                    self.uncertain_points.clear()
                    self.notify()
            elif len(self.uncertain_points) == 2:
                points = cur_main_center.points[-2:]
                points.extend(self.uncertain_points)
                self.main_centers.append(SameLevelMainCenter(points))
                self.uncertain_points.clear()
                self.notify()
            else:
                raise BaseException("invalid usage of MainCenterBuilder")

    def notify(self):
        for listener in self.listeners:
            listener.receive_maincenter(self.main_centers[-1])
