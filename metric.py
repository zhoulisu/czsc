from czsc.quote import QuoteEventListener, Quote, QuoteLevel
from typing import List


class MAQuery(QuoteEventListener):

    def __init__(self, period: int):
        self.period = period
        self.quotes_maintain: List[float] = []
        self.cur_ma = None

    def receive_raw_quote(self, quote: Quote, level: QuoteLevel):
        self.quotes_maintain.append(quote.close)
        if len(self.quotes_maintain) >= self.period:
            self.quotes_maintain = self.quotes_maintain[-self.period:]
            self.cur_ma = sum(self.quotes_maintain) / self.period
