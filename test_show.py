from typing import Dict
from typing import List

import numpy as np
import pyecharts.options as opts
from czsc.drawing import DrawingBuilder
from czsc.jq import get_all_quotes
from czsc.maincenter import SameLevelMainCenter
from czsc.maincenter import SameLevelMainCenterBuilder
from czsc.profit import ProfitCaculator
from czsc.quote import *
from czsc.segment import SegmentBuilder
from czsc.trader import EscapeMaincenterTrader
from czsc.trader import SameLevelTrendAchievedTrader
from czsc.trader import OnTrendSameLevelTrendAchievedTrader
from czsc.trader import ThirdSubTrendActiveTrader
from czsc.trader import TradeDirect
from czsc.trader import TradeType
from czsc.trend import ImmediateTrendBuilder
from czsc.trend import ThreeOverlapTrendBuilder
from czsc.metric import MAQuery
from pyecharts.charts import Candlestick
from pyecharts.charts import EffectScatter
from pyecharts.charts import Line
from pyecharts.charts import Bar
from pyecharts.charts import Grid
from pyecharts.globals import SymbolType
from pyecharts.commons.utils import JsCode
from talib import MA
from talib import MACD


def build_raw_quotes(candle, quotes, level):
    x_data = [quote.timestamp for quote in quotes]
    y_data = [[quote.open, quote.close, quote.low, quote.high] for quote in quotes]
    candle.add_xaxis(xaxis_data=x_data)
    candle.add_yaxis(series_name="raw_quotes_" + level.label, y_axis=y_data)
    candle.set_series_opts()
    candle.set_global_opts(
        xaxis_opts=opts.AxisOpts(is_scale=True),
        yaxis_opts=opts.AxisOpts(
            is_scale=True,
            splitarea_opts=opts.SplitAreaOpts(
                is_show=True, areastyle_opts=opts.AreaStyleOpts(opacity=1)
            ),
        ),
        datazoom_opts=[opts.DataZoomOpts(type_="inside")],
        title_opts=opts.TitleOpts(title="Kline-ItemStyle"),
    )


def build_ma(candle, quotes, close, level, multipler):
    x_data = [quote.timestamp for quote in quotes]
    ma = np.nan_to_num(MA(close, multipler).values)
    line = Line()
    line.add_xaxis(xaxis_data=x_data)
    line.add_yaxis(series_name="ma_100_" + level.label, y_axis=ma)
    candle.overlap(line)


def build_drawings(candle, drawings, level):
    line = Line()
    x_drawing = [p.quote.timestamp for p in drawings]
    y_drawing = [p.value() for p in drawings]
    line.add_xaxis(xaxis_data=x_drawing)
    line.add_yaxis(series_name=level.label + "_draw", y_axis=y_drawing)
    candle.overlap(line)


def build_segments(candle, segments, level):
    trend_line = Line()
    x_segments = [p.quote.timestamp for p in segments]
    y_segments = [p.value() for p in segments]
    trend_line.add_xaxis(xaxis_data=x_segments)
    trend_line.add_yaxis(series_name=level.label, y_axis=y_segments)
    candle.overlap(trend_line)


def build_trends(candle, trends, level):
    trend_line = Line()
    x_trends = [p.quote.timestamp for p in trends]
    y_trends = [p.value() for p in trends]
    trend_line.add_xaxis(xaxis_data=x_trends)
    trend_line.add_yaxis(series_name=level.label, y_axis=y_trends)
    candle.overlap(trend_line)


def build_trader(candle, trader: EscapeMaincenterTrader):
    long_trades = [trade for trade in trader.trades if
                   trade.type is TradeType.OPEN and trade.direct is TradeDirect.LONG]
    short_trades = [trade for trade in trader.trades if
                    trade.type is TradeType.OPEN and trade.direct is TradeDirect.SHORT]
    close_trades = [trade for trade in trader.trades if trade.type is TradeType.CLOSE]

    trade_point_chart_Long = EffectScatter()
    trade_point_chart_Long.add_xaxis([trade.point.quote.timestamp for trade in long_trades])
    trade_point_chart_Long.add_yaxis(trader.trend_level.label + "_trade",
                                     [trade.point.value() for trade in long_trades],
                                     symbol=SymbolType.ARROW)
    candle.overlap(trade_point_chart_Long)

    trade_point_chart_short = EffectScatter()
    trade_point_chart_short.add_xaxis([trade.point.quote.timestamp for trade in short_trades])
    trade_point_chart_short.add_yaxis(trader.trend_level.label + "_trade",
                                      [trade.point.value() for trade in short_trades],
                                      symbol=SymbolType.TRIANGLE)
    candle.overlap(trade_point_chart_short)

    trade_point_chart_close = EffectScatter()
    trade_point_chart_close.add_xaxis([trade.point.quote.timestamp for trade in close_trades])
    trade_point_chart_close.add_yaxis(trader.trend_level.label + "_trade",
                                      [trade.point.value() for trade in close_trades])
    candle.overlap(trade_point_chart_close)


def build_main_center(candle, level: QuoteLevel, main_centers: List[SameLevelMainCenter]):
    for maincenter in main_centers:
        rectangle = Line()
        start = maincenter.points[0].quote.timestamp
        end = maincenter.points[-1].quote.timestamp
        x = [start, end, end, start, start]
        y = [maincenter.bottom, maincenter.bottom, maincenter.top, maincenter.top, maincenter.bottom]
        rectangle.add_xaxis(xaxis_data=x)
        rectangle.add_yaxis(y_axis=y, series_name="maincenter_" + level.label)
        candle.overlap(rectangle)


def show_drawings(name: str, contract, level: QuoteLevel):
    quotes, _ = get_all_quotes(contract, level)
    candle = Candlestick(init_opts=opts.InitOpts(width="1300px", height="600px"))
    build_raw_quotes(candle, quotes, level)
    drawing_builder = DrawingBuilder(level)
    for quote in quotes:
        drawing_builder.receive_raw_quote(quote, level)
    drawings = drawing_builder.drawing_points
    build_drawings(candle, drawings, level)
    candle.render(name)


def show_segments(name: str, contract, level: QuoteLevel):
    quotes, _ = get_all_quotes(contract, level)
    candle = Candlestick(init_opts=opts.InitOpts(width="1300px", height="600px"))
    build_raw_quotes(candle, quotes, level)
    segment_builder = SegmentBuilder(level=level)
    drawing_builder = DrawingBuilder(level, [segment_builder])
    for quote in quotes:
        drawing_builder.receive_raw_quote(quote, level)
    drawings = drawing_builder.drawing_points
    build_drawings(candle, drawings, level)
    segments = segment_builder.segments;
    build_segments(candle, segments, level)
    candle.render(name)


def construct_trend_builder(level: QuoteLevel, trend_builder_map: Dict[QuoteLevel, ImmediateTrendBuilder]):
    if level is None:
        return None
    next_level_builder = construct_trend_builder(level.next_level, trend_builder_map)
    listeners = []
    if next_level_builder is not None:
        listeners.append(next_level_builder)
    builder: ImmediateTrendBuilder = ImmediateTrendBuilder(level, listeners)
    trend_builder_map[level] = builder
    return builder


def show_trends(name, contract, level: QuoteLevel):
    quotes, raw_df = get_all_quotes(contract, level)
    candle = Candlestick(init_opts=opts.InitOpts(width="1300px", height="600px"))
    build_raw_quotes(candle, quotes, level)
    trend_builder_map: {QuoteLevel: ThreeOverlapTrendBuilder} = {}
    next_level_trend_builder = construct_trend_builder(level.next_level, trend_builder_map)
    segment_builder = SegmentBuilder(level, [next_level_trend_builder])
    drawing_builder = DrawingBuilder(level, [segment_builder])
    for quote in quotes:
        drawing_builder.receive_raw_quote(quote, level)
    drawings = drawing_builder.drawing_points
    build_drawings(candle, drawings, level)
    segments = segment_builder.segments;
    build_segments(candle, segments, level)
    for l, builder in trend_builder_map.items():
        build_trends(candle, builder.trends, l)
    candle.render(name)



def show_traders(name, contract, level: QuoteLevel):
    quotes, raw_df = get_all_quotes(contract, level)
    close = raw_df['close']
    candle = Candlestick(init_opts=opts.InitOpts(width="1300px", height="600px"))
    # candle.set_global_opts(
    #     title_opts=opts.TitleOpts(title="K线周期图表", pos_left="0"),
    #     #    yaxis_opts=opts.AxisOpts(is_scale=True,splitarea_opts=opts.SplitAreaOpts(is_show=True, areastyle_opts=opts.AreaStyleOpts(opacity=1)),),
    #     yaxis_opts=opts.AxisOpts(is_scale=True, splitline_opts=opts.SplitLineOpts(is_show=True)),
    #     #    tooltip_opts=opts.TooltipOpts(trigger="axis", axis_pointer_type="line"),
    #     datazoom_opts=[
    #         opts.DataZoomOpts(is_show=False, type_="inside", xaxis_index=[0, 0], range_end=100),
    #         # xaxis_index=[0, 0]设置第一幅图为内部缩放
    #         opts.DataZoomOpts(is_show=True, xaxis_index=[0, 1], pos_top="97%", range_end=100),
    #         # xaxis_index=[0, 1]连接第二幅图的axis
    #         # opts.DataZoomOpts(is_show=False, xaxis_index=[0, 2], range_end=100),
    #         # # xaxis_index=[0, 2]连接第三幅图的axis
    #     ],
    #     # 三个图的 axis 连在一块
    #     # axispointer_opts=opts.AxisPointerOpts(
    #     #     is_show=True,
    #     #     link=[{"xAxisIndex": "all"}],
    #     #     label=opts.LabelOpts(background_color="#777"),
    #     # ),
    # )
    build_raw_quotes(candle, quotes, level)
    build_ma(candle, quotes, close, ONE_MINUTE, 100)
    build_ma(candle, quotes, close, FIVE_MINUTE, 100 * 5)
    build_ma(candle, quotes, close, THIRTY_MINUTE, 100 * 30)
    trend_builder_map: {QuoteLevel: ThreeOverlapTrendBuilder} = {}
    next_level_trend_builder = construct_trend_builder(level.next_level, trend_builder_map)
    segment_builder = SegmentBuilder(level, [next_level_trend_builder])
    drawing_builder = DrawingBuilder(level, [segment_builder])

    trader_map: {QuoteLevel: SameLevelTrendAchievedTrader} = {}
    cur_level: QuoteLevel = level.next_level
    while cur_level.next_level is not None:
        trader = SameLevelTrendAchievedTrader(cur_level)
        trend_builder_map[cur_level].listeners.append(trader)
        trend_builder_map[cur_level.next_level].listeners.append(trader)
        trader_map[cur_level.next_level] = trader
        cur_level = cur_level.next_level
    trader = SameLevelTrendAchievedTrader(level)
    segment_builder.listeners.append(trader)
    trend_builder_map[level.next_level].listeners.append(trader)
    trader_map[level.next_level] = trader

    for quote in quotes:
        drawing_builder.receive_raw_quote(quote, level)
    drawings = drawing_builder.drawing_points
    build_drawings(candle, drawings, level)
    segments = segment_builder.segments;
    build_segments(candle, segments, level)

    for l, builder in trend_builder_map.items():
        build_trends(candle, builder.trends, l)

    for l, trader in trader_map.items():
        build_trader(candle, trader)

    candle.render(name)
    # thirty_quotes, raw_df = get_all_quotes(contract, THIRTY_MINUTE)
    # macd = MACD(raw_df['close'], fastperiod=34, slowperiod=200, signalperiod=13)
    # real_macd = macd[2].fillna(0)
    # real_macd = [x for x in real_macd for i in range(30)]
    # x_data = [quote.timestamp for quote in quotes]
    # macd_bar = Bar()
    # macd_bar.add_xaxis(xaxis_data=x_data)
    # macd_bar.add_yaxis(
    #     series_name="MACD",
    #     y_axis=list(real_macd),
    #     xaxis_index=1,  # 用于合并显示时排列位置，单独显示不要添加
    #     yaxis_index=1,  # 用于合并显示时排列位置，单独显示不要添加
    #     label_opts=opts.LabelOpts(is_show=False),
    #     itemstyle_opts=opts.ItemStyleOpts(
    #         color=JsCode(
    #             """
    #                 function(params) {
    #                     var colorList;
    #                     if (params.data >= 0) {
    #                       colorList = '#ef232a';
    #                     } else {
    #                       colorList = '#14b143';
    #                     }
    #                     return colorList;
    #                 }
    #                 """
    #         )
    #     ),
    # )
    # macd_bar.set_global_opts(
    #     xaxis_opts=opts.AxisOpts(
    #         type_="category",
    #         grid_index=1,  # 用于合并显示时排列位置，单独显示不要添加
    #         axislabel_opts=opts.LabelOpts(is_show=False),
    #     ),
    #     yaxis_opts=opts.AxisOpts(
    #         grid_index=1,  # 用于合并显示时排列位置，单独显示不要添加
    #         split_number=4,
    #         axisline_opts=opts.AxisLineOpts(is_on_zero=False),
    #         axistick_opts=opts.AxisTickOpts(is_show=False),
    #         splitline_opts=opts.SplitLineOpts(is_show=False),
    #         axislabel_opts=opts.LabelOpts(is_show=True),
    #     ),
    #     legend_opts=opts.LegendOpts(is_show=False),
    # )
    #
    # grid_chart = Grid(init_opts=opts.InitOpts(width="1300px", height="600px"))
    # grid_chart.add(
    #     candle,
    #     grid_opts=opts.GridOpts(pos_left="3%", pos_right="1%", height="60%"),
    # )
    # grid_chart.add(
    #     macd_bar,
    #     grid_opts=opts.GridOpts(
    #         pos_left="3%", pos_right="1%", pos_top="75%", height="14%"
    #     ),
    # )
    #
    # # show
    # grid_chart.render(name)


def show_second_trade_point_trader(name, contract):
    quotes, raw_df = get_all_quotes(contract, ONE_MINUTE)
    close = raw_df['close']
    candle = Candlestick(init_opts=opts.InitOpts(width="1300px", height="600px"))

    build_raw_quotes(candle, quotes, ONE_MINUTE)
    trend_builder_map: Dict[QuoteLevel: ImmediateTrendBuilder] = {}
    next_level_trend_builder = construct_trend_builder(ONE_MINUTE.next_level, trend_builder_map)
    segment_builder = SegmentBuilder(ONE_MINUTE, [next_level_trend_builder])
    drawing_builder = DrawingBuilder(ONE_MINUTE, [segment_builder])
    ma_query = MAQuery(100 * 5)

    main_center_builder_map: Dict[QuoteLevel, SameLevelMainCenterBuilder] = {}

    for l, builder in trend_builder_map.items():
        main_center_builder = SameLevelMainCenterBuilder(l, [])
        builder.listeners.append(main_center_builder)
        main_center_builder_map[l] = main_center_builder

    thirty_min_trader = EscapeMaincenterTrader(FIVE_MINUTE)
    main_center_builder_map[THIRTY_MINUTE].listeners.append(thirty_min_trader)
    trend_builder_map[FIVE_MINUTE].listeners.append(thirty_min_trader)
    trend_builder_map[THIRTY_MINUTE].listeners.append(thirty_min_trader)

    five_min_trader = EscapeMaincenterTrader(ONE_MINUTE)
    main_center_builder_map[FIVE_MINUTE].listeners.append(five_min_trader)
    segment_builder.listeners.append(five_min_trader)
    trend_builder_map[FIVE_MINUTE].listeners.append(five_min_trader)

    for quote in quotes:
        ma_query.receive_raw_quote(quote, ONE_MINUTE)
        drawing_builder.receive_raw_quote(quote, ONE_MINUTE)

    drawings = drawing_builder.drawing_points
    build_drawings(candle, drawings, ONE_MINUTE)
    segments = segment_builder.segments;
    build_segments(candle, segments, ONE_MINUTE)

    for l, builder in trend_builder_map.items():
        build_trends(candle, builder.trends, l)

    for l, builder in main_center_builder_map.items():
        build_main_center(candle, l, builder.main_centers)

    build_trader(candle, thirty_min_trader)
    build_trader(candle, five_min_trader)

    calculator_thirty = ProfitCaculator(thirty_min_trader.trades)
    calculator_thirty.caculate()
    calculator_thirty.get_result()

    calculator_five = ProfitCaculator(five_min_trader.trades)
    calculator_five.caculate()
    calculator_five.get_result()

    candle.render(name)


# 趋势成立时开仓（平上一次开仓），每次低级别反向时减仓
def show_same_level_trade_point_trader(name, contract):
    quotes, raw_df = get_all_quotes(contract, ONE_MINUTE)
    candle = Candlestick(init_opts=opts.InitOpts(width="1300px", height="600px"))
    close = raw_df['close']
    # build_ma(candle, quotes, close, ONE_MINUTE, 100)
    build_ma(candle, quotes, close, FIVE_MINUTE, 100 * 5)
    build_ma(candle, quotes, close, THIRTY_MINUTE, 100 * 5 * 6)

    build_raw_quotes(candle, quotes, ONE_MINUTE)
    trend_builder_map: Dict[QuoteLevel: ImmediateTrendBuilder] = {}

    def construct_trend_builder(level: QuoteLevel,
                                trend_builder_map: Dict[QuoteLevel, ImmediateTrendBuilder]):
        if level is None:
            return None
        next_level_builder = construct_trend_builder(level.next_level, trend_builder_map)
        listeners = []
        if next_level_builder is not None:
            listeners.append(next_level_builder)
        builder: ImmediateTrendBuilder = ImmediateTrendBuilder(level, listeners, immedite=False)
        trend_builder_map[level] = builder
        return builder

    next_level_trend_builder = construct_trend_builder(ONE_MINUTE.next_level, trend_builder_map)
    segment_builder = SegmentBuilder(ONE_MINUTE, [next_level_trend_builder], immedite=False)
    drawing_builder = DrawingBuilder(ONE_MINUTE, [segment_builder])
    ma_query = MAQuery(100 * 5)

    thirty_min_trader = SameLevelTrendAchievedTrader(FIVE_MINUTE)
    trend_builder_map[FIVE_MINUTE].listeners.append(thirty_min_trader)
    trend_builder_map[THIRTY_MINUTE].listeners.append(thirty_min_trader)

    five_min_trader = SameLevelTrendAchievedTrader(ONE_MINUTE)
    segment_builder.listeners.append(five_min_trader)
    trend_builder_map[FIVE_MINUTE].listeners.append(five_min_trader)

    for quote in quotes:
        ma_query.receive_raw_quote(quote, ONE_MINUTE)
        drawing_builder.receive_raw_quote(quote, ONE_MINUTE)

    drawings = drawing_builder.drawing_points
    build_drawings(candle, drawings, ONE_MINUTE)
    segments = segment_builder.segments;
    build_segments(candle, segments, ONE_MINUTE)

    for l, builder in trend_builder_map.items():
        build_trends(candle, builder.trends, l)

    build_trader(candle, thirty_min_trader)
    build_trader(candle, five_min_trader)

    candle.render(name)
    print("30m:")
    calculator_thirty = ProfitCaculator(thirty_min_trader.trades)
    calculator_thirty.caculate()
    calculator_thirty.get_result()
    print("5m:")
    calculator_five = ProfitCaculator(five_min_trader.trades)
    calculator_five.caculate()
    calculator_five.get_result()


def test_macd(name, contract):
    quotes, raw_df = get_all_quotes(contract, ONE_MINUTE)

    kline = (
        Kline()
            .add_xaxis(xaxis_data=data["times"])
            .add_yaxis(
            series_name="",
            y_axis=data["datas"],
            itemstyle_opts=opts.ItemStyleOpts(
                color="#ef232a",
                color0="#14b143",
                border_color="#ef232a",
                border_color0="#14b143",
            ),
            markpoint_opts=opts.MarkPointOpts(
                data=[
                    opts.MarkPointItem(type_="max", name="最大值"),
                    opts.MarkPointItem(type_="min", name="最小值"),
                ]
            ),
            markline_opts=opts.MarkLineOpts(
                label_opts=opts.LabelOpts(
                    position="middle", color="blue", font_size=15
                ),
                data=split_data_part(),
                symbol=["circle", "none"],
            ),
        )
            .set_series_opts(
            markarea_opts=opts.MarkAreaOpts(is_silent=True, data=split_data_part())
        )
            .set_global_opts(
            title_opts=opts.TitleOpts(title="K线周期图表", pos_left="0"),
            xaxis_opts=opts.AxisOpts(
                type_="category",
                is_scale=True,
                boundary_gap=False,
                axisline_opts=opts.AxisLineOpts(is_on_zero=False),
                splitline_opts=opts.SplitLineOpts(is_show=False),
                split_number=20,
                min_="dataMin",
                max_="dataMax",
            ),
            yaxis_opts=opts.AxisOpts(
                is_scale=True, splitline_opts=opts.SplitLineOpts(is_show=True)
            ),
            tooltip_opts=opts.TooltipOpts(trigger="axis", axis_pointer_type="line"),
            datazoom_opts=[
                opts.DataZoomOpts(
                    is_show=False, type_="inside", xaxis_index=[0, 0], range_end=100
                ),
                opts.DataZoomOpts(
                    is_show=True, xaxis_index=[0, 1], pos_top="97%", range_end=100
                ),
                opts.DataZoomOpts(is_show=False, xaxis_index=[0, 2], range_end=100),
            ],
            # 三个图的 axis 连在一块
            # axispointer_opts=opts.AxisPointerOpts(
            #     is_show=True,
            #     link=[{"xAxisIndex": "all"}],
            #     label=opts.LabelOpts(background_color="#777"),
            # ),
        )
    )

    kline_line = (
        Line()
            .add_xaxis(xaxis_data=data["times"])
            .add_yaxis(
            series_name="MA5",
            y_axis=calculate_ma(day_count=5),
            is_smooth=True,
            linestyle_opts=opts.LineStyleOpts(opacity=0.5),
            label_opts=opts.LabelOpts(is_show=False),
        )
            .set_global_opts(
            xaxis_opts=opts.AxisOpts(
                type_="category",
                grid_index=1,
                axislabel_opts=opts.LabelOpts(is_show=False),
            ),
            yaxis_opts=opts.AxisOpts(
                grid_index=1,
                split_number=3,
                axisline_opts=opts.AxisLineOpts(is_on_zero=False),
                axistick_opts=opts.AxisTickOpts(is_show=False),
                splitline_opts=opts.SplitLineOpts(is_show=False),
                axislabel_opts=opts.LabelOpts(is_show=True),
            ),
        )
    )
    # Overlap Kline + Line
    overlap_kline_line = kline.overlap(kline_line)

    # Bar-1
    bar_1 = Bar()
    bar_1.add_xaxis(xaxis_data=data["times"]).add_yaxis(
        series_name="Volumn",
        y_axis=data["vols"],
        xaxis_index=1,
        yaxis_index=1,
        label_opts=opts.LabelOpts(is_show=False),
        # 根据 echarts demo 的原版是这么写的
        # itemstyle_opts=opts.ItemStyleOpts(
        #     color=JsCode("""
        #     function(params) {
        #         var colorList;
        #         if (data.datas[params.dataIndex][1]>data.datas[params.dataIndex][0]) {
        #           colorList = '#ef232a';
        #         } else {
        #           colorList = '#14b143';
        #         }
        #         return colorList;
        #     }
        #     """)
        # )
        # 改进后在 grid 中 add_js_funcs 后变成如下
        itemstyle_opts=opts.ItemStyleOpts(
            color=JsCode(
                """
            function(params) {
                var colorList;
                if (barData[params.dataIndex][1] > barData[params.dataIndex][0]) {
                    colorList = '#ef232a';
                } else {
                    colorList = '#14b143';
                }
                return colorList;
            }
            """
            )
        ),
    ).set_global_opts(
        xaxis_opts=opts.AxisOpts(
            type_="category",
            grid_index=1,
            axislabel_opts=opts.LabelOpts(is_show=False),
        ),
        legend_opts=opts.LegendOpts(is_show=False),
    )

    # Bar-2 (Overlap Bar + Line)
    bar_2 = Bar()
    bar_2.add_xaxis(xaxis_data=data["times"]).add_yaxis(
        series_name="MACD",
        y_axis=data["macds"],
        xaxis_index=2,
        yaxis_index=2,
        label_opts=opts.LabelOpts(is_show=False),
        itemstyle_opts=opts.ItemStyleOpts(
            color=JsCode(
                """
                    function(params) {
                        var colorList;
                        if (params.data >= 0) {
                          colorList = '#ef232a';
                        } else {
                          colorList = '#14b143';
                        }
                        return colorList;
                    }
                    """
            )
        ),
    ).set_global_opts(
        xaxis_opts=opts.AxisOpts(
            type_="category",
            grid_index=2,
            axislabel_opts=opts.LabelOpts(is_show=False),
        ),
        yaxis_opts=opts.AxisOpts(
            grid_index=2,
            split_number=4,
            axisline_opts=opts.AxisLineOpts(is_on_zero=False),
            axistick_opts=opts.AxisTickOpts(is_show=False),
            splitline_opts=opts.SplitLineOpts(is_show=False),
            axislabel_opts=opts.LabelOpts(is_show=True),
        ),
        legend_opts=opts.LegendOpts(is_show=False),
    )

    line_2 = Line()
    line_2.add_xaxis(xaxis_data=data["times"]).add_yaxis(
        series_name="DIF",
        y_axis=data["difs"],
        xaxis_index=2,
        yaxis_index=2,
        label_opts=opts.LabelOpts(is_show=False),
    ).add_yaxis(
        series_name="DIF",
        y_axis=data["deas"],
        xaxis_index=2,
        yaxis_index=2,
        label_opts=opts.LabelOpts(is_show=False),
    ).set_global_opts(legend_opts=opts.LegendOpts(is_show=False))

    # 最下面的柱状图和折线图
    overlap_bar_line = bar_2.overlap(line_2)

    # 最后的 Grid
    grid_chart = Grid(init_opts=opts.InitOpts(width="1400px", height="800px"))

    # 这个是为了把 data.datas 这个数据写入到 html 中,还没想到怎么跨 series 传值
    # demo 中的代码也是用全局变量传的
    grid_chart.add_js_funcs("var barData = {}".format(data["datas"]))

    # K线图和 MA5 的折线图
    grid_chart.add(
        overlap_kline_line,
        grid_opts=opts.GridOpts(pos_left="3%", pos_right="1%", height="60%"),
    )
    # Volumn 柱状图
    grid_chart.add(
        bar_1,
        grid_opts=opts.GridOpts(
            pos_left="3%", pos_right="1%", pos_top="71%", height="10%"
        ),
    )
    # MACD DIFS DEAS
    grid_chart.add(
        overlap_bar_line,
        grid_opts=opts.GridOpts(
            pos_left="3%", pos_right="1%", pos_top="82%", height="14%"
        ),
    )
    grid_chart.render("professional_kline_chart.html")



if __name__ == '__main__':
    show_same_level_trade_point_trader("show-drawings-RB8888.html", "RB8888.XSGE")
    # test_macd("show-macd-RB8888.html","RB8888.XSGE")