import configparser
from datetime import date
from typing import Dict

from czsc.drawing import DrawingBuilder
from czsc.profit import ProfitCaculator
from czsc.quote import *
from czsc.segment import SegmentBuilder
from czsc.trader import AbstractTradeListener
from czsc.trader import SameLevelTrendAchievedTrader
from czsc.trader import Trade
from czsc.trader import TradeDirect
from czsc.trader import TradeType
from czsc.trend import ImmediateTrendBuilder
from tqsdk import BacktestFinished
from tqsdk import TqApi, TqAuth, tafunc, TqBacktest, TqSim
from tqsdk.lib import TargetPosTask
from tqsdk.objs import Position

config = configparser.ConfigParser()
config.read("tq.conf", encoding="utf-8")
tq_account = config.get('tq', 'id')
tq_passwd = config.get('tq', 'passwd')

open_volumns_per_100000 = {
    'rb': 8,
    'ag': 8,
}


class TqOMS(AbstractTradeListener):

    def __init__(self, api: TqApi, symbol: str, open_volumn: int):
        self.api: TqApi = api
        self.open_volumn: int = open_volumn
        self.symbol = symbol
        self.task: TargetPosTask = TargetPosTask(api, symbol, offset_priority="今昨,开")

    def receive_trace(self, trade: Trade):
        position: Position = self.api.get_position(self.symbol)
        print("current balance: {}".format(self.api.get_account().balance))
        print("reveive trade {}, {}, {}, with cur position {}".format(trade.type, trade.direct, trade.proportion,
                                                                      position.pos))
        # todo 持仓合法性检查
        if trade.type is TradeType.OPEN:

            self.task.set_target_volume(self.open_volumn if trade.direct is TradeDirect.LONG else -self.open_volumn)
        else:
            # 目前仅单向开仓
            self.task.set_target_volume(int(position.pos * (1 - trade.proportion)))


def convert_to_quote(kline):
    return Quote(kline['open'], kline['close'], kline['high'], kline['low'], kline['volume'],
                 tafunc.time_to_datetime(kline["datetime"]))


def build_trader(api: TqApi, symbol: str) -> QuoteEventListener:
    trend_builder_map: Dict[QuoteLevel: ImmediateTrendBuilder] = {}

    def construct_trend_builder(level: QuoteLevel,
                                trend_builder_map: Dict[QuoteLevel, ImmediateTrendBuilder]):
        if level is None or level is ONE_DAY:
            return None
        next_level_builder = construct_trend_builder(level.next_level, trend_builder_map)
        listeners = []
        if next_level_builder is not None:
            listeners.append(next_level_builder)
        builder: ImmediateTrendBuilder = ImmediateTrendBuilder(level, listeners, immedite=False)
        trend_builder_map[level] = builder
        return builder

    next_level_trend_builder = construct_trend_builder(ONE_MINUTE.next_level, trend_builder_map)
    segment_builder = SegmentBuilder(ONE_MINUTE, [next_level_trend_builder], immedite=False)
    drawing_builder = DrawingBuilder(ONE_MINUTE, [segment_builder])
    five_min_trader = SameLevelTrendAchievedTrader(ONE_MINUTE)
    segment_builder.listeners.append(five_min_trader)
    trend_builder_map[FIVE_MINUTE].listeners.append(five_min_trader)
    oms = TqOMS(api, symbol, 8)
    five_min_trader.trade_listeners.append(oms)
    # thirty_min_trader = SameLevelTrendAchievedTrader(FIVE_MINUTE)
    # trend_builder_map[FIVE_MINUTE].listeners.append(thirty_min_trader)
    # trend_builder_map[THIRTY_MINUTE].listeners.append(thirty_min_trader)
    # oms = TqOMS(api, symbol, 8)
    # thirty_min_trader.trade_listeners.append(oms)
    return drawing_builder, five_min_trader


def run_on_tq_api(api: TqApi, quote_listener: QuoteEventListener, symbol: str):
    klines = api.get_kline_serial(symbol, 60, 1)
    last_timestamp: datetime = None
    last_kline = None

    while api.wait_update():
        if api.is_changing(klines):
            kline = klines.iloc[-1]
            timestamp = tafunc.time_to_datetime(kline["datetime"])
            if last_timestamp is not None and last_timestamp.minute != timestamp.minute:
                close = last_kline["close"]
                quote = convert_to_quote(last_kline)
                # print("current time: {}".format(datetime.now().time()))
                quote_listener.receive_raw_quote(quote, ONE_MINUTE)
            last_timestamp = timestamp
            last_kline = kline


def run_on_online(symbol: str):
    api = TqApi(auth=TqAuth(tq_account, tq_passwd))
    quote_listener = build_trader(api, symbol)
    klines = api.get_kline_serial(tsymbol, duration_seconds=60, data_length=4096)
    # todo 预热不交易且启动后才交易
    run_on_tq_api(api, quote_listener, symbol)


def run_on_backtest(symbol: str):
    acc = TqSim(init_balance=400000)

    api = TqApi(account=acc, auth=TqAuth(tq_account, tq_passwd),
                backtest=TqBacktest(start_dt=date(2012, 1, 1), end_dt=date(2022, 2, 1)))
    quote_listener, trader = build_trader(api, symbol)
    try:
        run_on_tq_api(api, quote_listener, symbol)
    except BacktestFinished as e:
        api.close()
        print(acc.trade_log)  # 回测的详细信息

        print(acc.tqsdk_stat)  # 回测时间内账户交易信息统计结果，其中包含以下字段
        print("#######self ProfitCaculator result#######")
        caculator = ProfitCaculator(trader.trades)
        caculator.caculate()
        caculator.get_result()


if __name__ == '__main__':
    run_on_backtest('KQ.i@SHFE.rb')
