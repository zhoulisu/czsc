from typing import List

from czsc.base import DirectType
from czsc.base import PointType
from czsc.quote import Quote


class CzscPoint:
    """
    缠论笔段顶点
    原则上返回bool的比较方法，self均为时间轴上较早的点
    """

    def __init__(self, point_type: PointType, quote: Quote):
        self.point_type: PointType = point_type
        self.quote: Quote = quote

    def print(self):
        print(str(self.quote.timestamp) + ": " + str(self.point_type) + ": " +
              str(self.quote.high if self.point_type is PointType.TOP else self.quote.low))

    def value(self):
        return self.quote.high if self.point_type is PointType.TOP else self.quote.low

    def inner(self, point) -> bool:
        return point.value() < self.value() if self.point_type is PointType.TOP else point.value() > self.value()

    def outer(self, point) -> bool:
        return not self.inner(point)

    def continuous(self, point) -> bool:
        if self.point_type is not point.point_type:
            raise BaseException("invalid usage of CzscPoint::continuous")
        return self.value() < point.value() if self.point_type is PointType.TOP else self.value() > point.value()

    def break_through(self, point) -> bool:
        """
        跌穿或涨破，self和point要求相反类型
        :param point:
        :return:
        """
        if self.point_type.reverse() is not point.point_type:
            raise BaseException("invalid usage of CzscPoint::break_through")
        return point.value() < self.value() if self.point_type is PointType.TOP else point.value() > self.value()

    def over(self, edge_value: float) -> bool:
        """
        超过同方向边界值
        :param edge_value:
        :return:
        """
        return self.value() > edge_value if self.point_type is PointType.TOP else self.value() < edge_value


def extreme_func(direct: DirectType):
    return max if direct is DirectType.UP else min


def extreme_point(points: List[CzscPoint], direct: DirectType) -> CzscPoint:
    values = [p.value() for p in points]
    return [p for p in points if p.value() is extreme_func(direct)(values)][0]


def on_direct_point(quote: Quote, direct: DirectType) -> CzscPoint:
    return CzscPoint(PointType.TOP if direct is DirectType.UP else PointType.BOTTOM, quote)
