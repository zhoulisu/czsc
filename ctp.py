from vnpy.gateway.ctp import CtpGateway
from vnpy.trader.event import *
from vnpy.trader.engine import *
from vnpy.trader.object import BarData
from vnpy.trader.utility import BarGenerator
import configparser

ctp_setting = {
    '用户名': '',
    '密码': '',
    '经纪商代码': '9999',
    '交易服务器': 'tcp://180.168.146.187:10101',
    '行情服务器': 'tcp://180.168.146.187:10111',
    '产品名称': 'simnow_client_test',
    '授权编码': '0000000000000000',
    '产品信息': ''
}




def handle_bar(bar: BarData):
    pass


bar_generator = BarGenerator()


def init_engine():
    config = configparser.ConfigParser()
    config.read("simnow.conf", encoding="utf-8")
    user_id = config.get('simnow', 'userId')
    passwd = config.get('simnow', 'passwd')
    ctp_setting['用户名'] = user_id
    ctp_setting['密码'] = passwd
    event_engine = EventEngine()
    event_engine.register()
    ctp_gateway = CtpGateway(event_engine)
    ctp_gateway.connect(ctp_setting)

