from itertools import groupby
from typing import Dict

import numpy as np
from czsc.drawing import *
from czsc.quote import *
from czsc.segment import *
from czsc.trader import *
from czsc.trend import ImmediateThreeOverlapTrendBuilder


def MaxDrawdown(return_list):
    # 1. find all of the peak of cumlative return
    maxcum = np.zeros(len(return_list))
    b = return_list[0]
    for i in range(0, len((return_list))):
        if (return_list[i] > b):
            b = return_list[i]
        maxcum[i] = b

    # 2. then find the max drawndown point
    i = np.argmax(maxcum - return_list)
    if i == 0:
        return 0, 0
    j = np.argmax(return_list[:i])

    # 3. return the maxdrawndown
    return j, i


class CompleteTrade:
    """
    包含一次开仓及一次及以上平仓的完整交易
    每次平仓的proportion为当前仓位的比例
    最后一次比例一定为100%
    """

    def __init__(self, open: Trade, closes: List[Trade]):
        self.open: Trade = open
        self.closes: List[Trade] = closes
        self.profit: float = self.caculate_profit()

    def caculate_profit(self) -> float:
        cur_profit = 0
        cur_proportion = 1
        open_value = self.open.point.value()
        for close in self.closes:
            if cur_proportion <= 0:
                raise BaseException("invalid usage of ProfitCaculator")
            close_proportion = cur_proportion * close.proportion
            cur_profit += (close.point.value() - open_value) * close_proportion
            cur_proportion -= close_proportion
        if self.open.direct is TradeDirect.SHORT:
            cur_profit = -cur_profit
        return cur_profit

    def time(self):
        return self.open.point.quote.timestamp


class ProfitCaculator():

    def __init__(self, trades: List[Trade]):
        self.trades: List[Trade] = trades
        self.profits: List[CompleteTrade] = []
        self.loss: List[CompleteTrade] = []

    def get_result(self):

        def printPL(profits, loss):
            print("profit rate p/l: " + str(len(profits)) + ":" + str(len(loss)))
            profit_values = [x.profit for x in profits]
            loss_values = [x.profit for x in loss]
            if len(self.profits) != 0:
                print("average profit: " + str(sum(profit_values) / len(profits)))
                # print("std profit: " + str(np.std(np.array(profits))))
            if len(self.loss) != 0:
                print("average loss: " + str(sum(loss_values) / len(loss)))
                # print("std loss: " + str(np.std(np.array(loss))))
            print("expect total profit: " + str(sum(profit_values) + sum(loss_values)))
            all: List[CompleteTrade] = []
            all.extend(profits)
            all.extend(loss)
            all.sort(key=lambda x: x.time())
            # for ct in all:
            #     print("trade {} profit {} open {} direct {}".format(ct.time(), ct.profit, ct.open.point.value(),
            #                                                         ct.open.direct))
            #     for cl in ct.closes:
            #         print("close {} at price {}".format(cl.point.quote.timestamp, cl.point.value()))
            all_value = [x.profit for x in all]
            net_value = []
            cur_net = 0
            for i in range(len(all)):
                cur_net += all_value[i]
                net_value.append(cur_net)

            j, i = MaxDrawdown(net_value)
            print("MaxDrawdown: " + str(net_value[j] - net_value[i]))
            print("from {f} to {t}".format(f=all[j].time(), t=all[i].time()))

        printPL(self.profits, self.loss)

        year_seperated_profits = groupby(self.profits, lambda x: x.time().year)
        year_seperated_loss = groupby(self.loss, lambda x: x.time().year)
        year_profit_dict = {}
        year_loss_dict = {}
        for year, profits in year_seperated_profits:
            year_profit_dict[year] = list(profits)
        for year, loss in year_seperated_loss:
            year_loss_dict[year] = list(loss)
            print("year: " + str(year))
            printPL(year_profit_dict[year], year_loss_dict[year])

    def caculate(self):
        index = 0
        while index < len(self.trades):
            cur_open: Trade = self.trades[index]
            if cur_open.type is not TradeType.OPEN:
                raise BaseException("invalid usage of ProfitCaculator")
            cur_closes: List[Trade] = []
            index += 1
            while index < len(self.trades) and self.trades[index].type is TradeType.CLOSE:
                cur_closes.append(self.trades[index])
                index += 1
            if len(cur_closes) == 0 or cur_closes[-1].proportion != 1:
                print("incomplete trade open at " + str(cur_open.point.quote.timestamp) + " should be last trade")
                break
            complete_trade = CompleteTrade(cur_open, cur_closes)
            self.profits.append(complete_trade) if complete_trade.profit > 0 else self.loss.append(complete_trade)


def test_same_level_trade_point_trader(contract):
    quotes, _ = get_all_quotes(contract, ONE_MINUTE)
    trend_builder_map: Dict[QuoteLevel: ImmediateThreeOverlapTrendBuilder] = {}

    def construct_trend_builder(level: QuoteLevel,
                                trend_builder_map: Dict[QuoteLevel, ImmediateThreeOverlapTrendBuilder]):
        if level is None:
            return None
        next_level_builder = construct_trend_builder(level.next_level, trend_builder_map)
        listeners = []
        if next_level_builder is not None:
            listeners.append(next_level_builder)
        builder: ImmediateThreeOverlapTrendBuilder = ImmediateThreeOverlapTrendBuilder(level, listeners)
        trend_builder_map[level] = builder
        return builder

    next_level_trend_builder = construct_trend_builder(ONE_MINUTE.next_level, trend_builder_map)
    segment_builder = SegmentBuilder(ONE_MINUTE, [next_level_trend_builder])
    drawing_builder = DrawingBuilder(ONE_MINUTE, [segment_builder])

    thirty_min_trader = SameLevelTrendAchievedTrader(FIVE_MINUTE)
    trend_builder_map[FIVE_MINUTE].listeners.append(thirty_min_trader)
    trend_builder_map[THIRTY_MINUTE].listeners.append(thirty_min_trader)

    five_min_trader = SameLevelTrendAchievedTrader(ONE_MINUTE)
    segment_builder.listeners.append(five_min_trader)
    trend_builder_map[FIVE_MINUTE].listeners.append(five_min_trader)

    for quote in quotes:
        drawing_builder.receive_raw_quote(quote, ONE_MINUTE)

    print("test contract: " + contract)

    print("30m:")
    calculator_thirty = ProfitCaculator(thirty_min_trader.trades)
    calculator_thirty.caculate()
    calculator_thirty.get_result()

    print("5m:")
    calculator_five = ProfitCaculator(five_min_trader.trades)
    calculator_five.caculate()
    calculator_five.get_result()

    print("\n")


if __name__ == '__main__':
    # cached_files = os.listdir(cache_root_path)
    # cached_contracts = [x.split(':')[0] for x in cached_files]
    # for contract in cached_contracts:
    #     print("start test contract: " + contract)
    #     test_same_level_trade_point_trader(contract)
    # test_same_level_trade_point_trader("IH8888.CCFX")
    test_same_level_trade_point_trader("RB8888.XSGE")
