from abc import abstractmethod
from enum import Enum
from typing import List, Any

from czsc.abstractTrend import AbstractTrendListener, AbstractTrend
from czsc.abstractTrend import SimpleTrendImpl
from czsc.base import DirectType
from czsc.maincenter import MaincenterListener
from czsc.maincenter import SameLevelMainCenter
from czsc.metric import MAQuery
from czsc.point import CzscPoint
from czsc.quote import QuoteLevel
from czsc.trend import SameLevelBuiltTrend


class TradeType(Enum):
    OPEN = 'open'
    CLOSE = 'close'


class TradeDirect(Enum):
    LONG = 'long'
    SHORT = 'short'

    def reverse(self):
        return TradeDirect.LONG if self is TradeDirect.SHORT else TradeDirect.SHORT


def related_direct(direct: DirectType) -> TradeDirect:
    return TradeDirect.LONG if direct is DirectType.UP else TradeDirect.SHORT


class Trade():

    def __init__(self, type: TradeType, direct: TradeDirect, point: CzscPoint,
                 proportion: float = 1, open_context=None):
        self.type: TradeType = type
        self.direct: TradeDirect = direct
        self.point: CzscPoint = point
        self.proportion: float = proportion  # 平仓比例
        self.open_context = open_context


class AbstractTradeListener():

    @abstractmethod
    def receive_trace(self, trade: Trade):
        pass


def standard_three_trend_on_direct(points: List[CzscPoint]) -> bool:
    """
    判断标准上下上或下上下三段是否构成
    判断标准
    上：低点上涨，高点均可
    下：高点下跌，低点均可
    若构成，则可确定起点一定是高级别走势起点
    """
    if len(points) < 4:
        return False
    a0 = points[-4]
    a2 = points[-2]
    a3 = points[-1]
    return not a0.continuous(a2)


class SecondTradePointTrader(AbstractTrendListener):
    """
    如何判断某级别走势结束？ 反向走势成立或该级别的次级别走势方向触及该级别100均线

    so， 对于30min同级别分解，出现成立的两段5min走势后，1min走势突破5min 100均线，即认为第二段结束，
    第三段出现，二买成立；若在第三段5min走势前再次新低，则说明之前二买判断失败，原来第二段5min仍在延续，止损退出
    任何情况下1min走势破预设2买止损

    二买成立后，反向开仓成立则进入下个操作周期（todo， 确定是否还有别的case）
    """

    def __init__(self, predict_level: QuoteLevel, base_level_MA: MAQuery):
        self.trades: List[Trade] = []
        self.predict_level: QuoteLevel = predict_level
        self.base_level: QuoteLevel = predict_level.next_level
        if self.base_level is None:
            raise BaseException("invalid usage of SecondTradePointTrader")
        self.trend_level = self.base_level.next_level
        if self.trend_level is None:
            raise BaseException("invalid usage of SecondTradePointTrader")
        self.base_level_MA: MAQuery = base_level_MA
        self.last_received_trend: SameLevelBuiltTrend = None
        self.uncertain_base_level_trends_points: List[CzscPoint] = []
        self.last_open_predict_level_trend: AbstractTrend = None
        self.last_open_second_trade_point_trend: AbstractTrend = None

        self.on_open: bool = False
        self.close_edge: CzscPoint = None
        self.open_direct: TradeDirect = None

    def close(self, point: CzscPoint):
        self.trades.append(Trade(TradeType.CLOSE, None, point))
        self.close_edge = None
        self.on_open = False
        self.last_open_predict_level_trend = None
        self.last_open_second_trade_point_trend = None

    def receive_trend(self, trend: AbstractTrend):
        if trend.get_level() is self.trend_level:
            self.last_received_trend = trend
            self.uncertain_base_level_trends_points = [x for x in self.uncertain_base_level_trends_points if
                                                       x.quote.timestamp > trend.get_end().quote.timestamp]
        elif trend.get_level() is self.base_level:
            # 再次收到2买时base_level的trend，则说明预测2买失败，平仓
            if self.on_open:
                if trend.same(self.last_open_second_trade_point_trend):
                    self.close(trend.get_end())
                elif self.open_direct.reverse() is related_direct(trend.get_direct()) \
                        and self.close_edge.continuous(trend.get_end()):
                    self.close(trend.get_end())
            if self.last_received_trend is not None and trend.get_end().quote.timestamp \
                    <= self.last_received_trend.get_end().quote.timestamp:
                return

            if len(self.uncertain_base_level_trends_points) == 0:
                self.uncertain_base_level_trends_points.append(trend.get_end())
            elif self.uncertain_base_level_trends_points[-1] is trend.get_start():
                self.uncertain_base_level_trends_points.append(trend.get_end())
            else:
                self.uncertain_base_level_trends_points[-1] = trend.get_end()

        elif trend.get_level() is self.predict_level:
            """
            1. 已开仓predict_level不再处理
            2. uncertain_edge为2，尝试第二交易点
            3. 与last_trend反向
            4. 穿100MA
            5. 
            """
            if (self.last_open_predict_level_trend is None or not self.last_open_predict_level_trend.same(trend)) and \
                    self.last_received_trend is not None and len(self.uncertain_base_level_trends_points) == 2 and \
                    trend.get_direct() is self.last_received_trend.get_direct().reverse() and \
                    self.base_level_MA.cur_ma is not None and trend.get_end().over(self.base_level_MA.cur_ma):
                point = trend.get_end()
                if self.on_open:
                    self.close(point)
                open_direct: TradeDirect = related_direct(trend.get_direct())
                self.trades.append(Trade(TradeType.OPEN, open_direct, point))
                self.on_open = True
                self.close_edge = self.uncertain_base_level_trends_points[-1]
                self.open_direct = open_direct
                self.last_open_predict_level_trend = trend
                self.last_open_second_trade_point_trend = SimpleTrendImpl(self.uncertain_base_level_trends_points[-2],
                                                                          self.uncertain_base_level_trends_points[-1],
                                                                          self.base_level)


        else:
            raise BaseException("invalid usage of SecondTradePointTrader")


class SameLevelTrendAchievedTrader(AbstractTrendListener):
    """
    同级别分解下，新趋势成立时开仓（同时平上一笔），接近第二交易点
    每次次级别反向成立时减仓
    """

    def __init__(self, base_level: QuoteLevel, listeners: List[AbstractTradeListener] = []):
        self.last_received_trend: AbstractTrend = None
        self.last_received_base_level_trend: AbstractTrend = None
        self.trades: List[Trade] = []
        self.base_level = base_level  # 构建趋势的基础行情level
        self.trend_level = base_level.next_level  # 趋势level
        self.on_open: bool = False
        self.trade_direct: TradeDirect = None
        self.close_edge: CzscPoint = None
        self.trade_listeners: List[AbstractTradeListener] = listeners
        if self.trend_level is None:
            raise BaseException("invalid usage of StandardThreeAchievedTrader")

    def append_trade(self, trade: Trade):
        self.trades.append(trade)
        for listener in self.trade_listeners:
            listener.receive_trace(trade)

    def receive_trend(self, trend: AbstractTrend):
        if trend.get_level() is self.base_level:
            self.handle_base_level(trend)
        elif trend.get_level() is self.trend_level:
            # if trend.__class__ is not SameLevelBuiltTrend:
            #     print(trend.__class__)
            #     raise BaseException("invalid usage of StandardThreeAchievedTrader")
            self.handle_trend_level(trend)
        else:
            raise BaseException("invalid usage of StandardThreeAchievedTrader")

    def close(self, point: CzscPoint):
        self.append_trade(Trade(TradeType.CLOSE, None, point))
        self.close_edge = None
        self.on_open = False

    def reduce(self, point: CzscPoint, proportion: float):
        self.append_trade(Trade(TradeType.CLOSE, None, point, proportion))

    def handle_base_level(self, trend: AbstractTrend):
        point = trend.get_end()
        if self.on_open and related_direct(trend.get_direct()) is not self.trade_direct:
            if self.close_edge.continuous(point):
                # self.close(point)
                pass
            elif self.last_received_base_level_trend is not None and \
                    not self.last_received_base_level_trend.same(trend):
                pass
                self.reduce(point, 0.5)
        self.last_received_base_level_trend = trend

    def handle_trend_level(self, trend: SameLevelBuiltTrend):
        if self.last_received_trend is None or (not self.last_received_trend.same(trend)):
            point = trend.get_end()
            if self.on_open:
                self.append_trade(Trade(TradeType.CLOSE, None, point))
                self.on_open = False
            # if len(trend.lower_level_trend_points)>3:
            trade_direct = related_direct(trend.get_direct())
            self.append_trade(Trade(TradeType.OPEN, trade_direct, point))
            self.on_open = True
            self.trade_direct = trade_direct
            self.close_edge = trend.lower_level_trend_points[-2]
        self.last_received_trend = trend


class OnTrendSameLevelTrendAchievedTrader(SameLevelTrendAchievedTrader):

    def __init__(self, base_level: QuoteLevel, listeners: List[AbstractTradeListener] = []):
        super().__init__(base_level, listeners)
        self.trend_points: List[CzscPoint] = []

    def handle_trend_level(self, trend: SameLevelBuiltTrend):
        if self.last_received_trend is None or (not self.last_received_trend.same(trend)):
            self.trend_points.append(trend.get_start())
            self.trend_points = self.trend_points[-3:]
            point = trend.get_end()
            if self.on_open:
                self.append_trade(Trade(TradeType.CLOSE, None, point))
                self.on_open = False
            # if len(trend.lower_level_trend_points)>3:
            if len(self.trend_points) == 3 and not self.trend_points[-3].continuous(self.trend_points[-1]):
                trade_direct = related_direct(trend.get_direct())
                self.append_trade(Trade(TradeType.OPEN, trade_direct, point))
                self.on_open = True
                self.trade_direct = trade_direct
                self.close_edge = trend.lower_level_trend_points[-2]
        self.last_received_trend = trend


class ThirdSubTrendActiveTrader(SameLevelTrendAchievedTrader):

    def handle_trend_level(self, trend: SameLevelBuiltTrend):
        point = trend.get_end()
        if self.last_received_trend is None or (not self.last_received_trend.same(trend)):
            if self.on_open:
                self.append_trade(Trade(TradeType.CLOSE, None, point))
                self.on_open = False
        if (not self.on_open) and len(trend.lower_level_trend_points) == 6 and not trend.lower_level_trend_points[-4]. \
                continuous(trend.lower_level_trend_points[-2]):
            trade_direct = related_direct(trend.get_direct())
            self.append_trade(Trade(TradeType.OPEN, trade_direct, point))
            self.on_open = True
            self.trade_direct = trade_direct
            self.close_edge = trend.lower_level_trend_points[-2]
        self.last_received_trend = trend


class EscapeMaincenterTrader(AbstractTrendListener, MaincenterListener):
    def __init__(self, base_level: QuoteLevel):
        self.base_level = base_level
        self.trend_level = base_level.next_level
        if self.trend_level is None:
            raise BaseException("invalid usage of NoOverlapTrader")
        self.trades: List[Trade] = []
        self.last_maincenter: SameLevelMainCenter = None

        self.on_open: bool = False
        self.trade_direct: TradeDirect = None
        self.close_edge: CzscPoint = None
        self.last_open_trend: AbstractTrend = None

    def close(self, point: CzscPoint):
        self.trades.append(Trade(TradeType.CLOSE, None, point))
        self.close_edge = None
        self.on_open = False
        self.last_open_trend = None

    def receive_maincenter(self, maincenter: SameLevelMainCenter):
        if maincenter.__class__ is not SameLevelMainCenter:
            raise BaseException("EscapeMaincenterTrader")
        if self.last_maincenter is not None and not self.last_maincenter.same(maincenter):
            trade_point = maincenter.get_end()
            trade_direct = related_direct(trade_point.point_type.associate_direct())
            self.trades.append(Trade(TradeType.OPEN, trade_direct, trade_point))
            self.on_open = True
            self.close_edge = maincenter.points[-2].value()
            self.trade_direct = trade_direct
            self.last_open_trend = SimpleTrendImpl(maincenter.points[-2], maincenter.points[-1], self.trend_level)
        self.last_maincenter = maincenter

    def receive_trend(self, trend: AbstractTrend):
        if trend.get_level() is self.base_level:
            self.handle_base_level(trend)
        elif trend.get_level() is self.trend_level:
            self.handle_trend_level(trend)
        else:
            raise BaseException("invalid usage of NoOverlapTrader")

    def handle_base_level(self, trend: AbstractTrend):
        point = trend.get_end()
        if self.on_open:
            if related_direct(trend.get_direct()).reverse() is self.trade_direct and point.over(self.close_edge):
                self.close(point)
        else:
            # todo
            pass

    def handle_trend_level(self, trend: AbstractTrend):
        if self.on_open and not self.last_open_trend.same(trend):
            self.close(trend.get_end())
