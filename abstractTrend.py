from abc import ABCMeta
from abc import abstractmethod

from czsc.point import CzscPoint
from czsc.base import DirectType
from czsc.quote import QuoteLevel


class AbstractTrend(metaclass=ABCMeta):
    """
    走势抽象
    低级别走势重叠构成中枢，中枢又生成当级别走势
    抽象无中枢引用的趋势，防止循环依赖
    """

    @abstractmethod
    def get_level(self) -> QuoteLevel:
        pass

    @abstractmethod
    def get_start(self) -> CzscPoint:
        pass

    @abstractmethod
    def get_end(self) -> CzscPoint:
        pass

    @abstractmethod
    def get_direct(self) -> DirectType:
        pass

    def same(self, another) -> bool:
        return self.get_start() is another.get_start()


class SimpleTrendImpl(AbstractTrend):
    """
    抽象走势简单实现
    """

    def __init__(self, start: CzscPoint, end: CzscPoint, level: QuoteLevel):
        self.start = start
        self.end = end
        self.direct = DirectType.UP if end.value() > start.value() else DirectType.DOWN
        self.level = level;

    def get_level(self) -> QuoteLevel:
        return self.level

    def get_start(self) -> CzscPoint:
        return self.start

    def get_end(self) -> CzscPoint:
        return self.end

    def get_direct(self) -> DirectType:
        return self.direct


class AbstractTrendListener:
    """
    走势事件监听器
    """

    @abstractmethod
    def receive_trend(self, trend: AbstractTrend):
        """
        接收走势通知事件
        新接收的trend可能是上一次接收的更新，接收方需要通过AbstractTrend#same()判断
        保证新走势起点为上一走势终点
        :param trend: 缠论走势
        :return:
        """
        pass
