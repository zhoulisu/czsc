import os
import pandas as pd

from czsc import cache_root_path
from czsc import local_mod
from czsc import mod
from czsc.quote import *
from jqdatasdk import *


def format_quotes(df: pd.DataFrame):
    quote_list = list()
    for (time, price) in df.iterrows():
        quote_list.append(Quote(
            open_price=price['open'],
            close_price=price['close'],
            high_price=price['high'],
            low_price=price['low'],
            volume=price['volume'],
            timestamp=time
        ))
    return quote_list


def get_quotes(contract, end_time: datetime, count: int, level: QuoteLevel):
    contract = normalize_code(contract)
    df = get_price(contract, end_date=end_time, frequency=level.label, count=count, skip_paused=True, fill_paused=False)
    return format_quotes(df)


def get_all_quotes(contract, level: QuoteLevel):
    if mod != local_mod:
        contract = normalize_code(contract)
    cache_file_path = get_cache_path(contract, level)
    df_final = None
    if os.path.exists(cache_file_path):
        df_cached = pd.read_csv(cache_file_path, index_col=0, parse_dates=True)
        last_idx = df_cached.index[-1]
        if mod != local_mod:
            df_query = get_price(contract, start_date=last_idx.date(), end_date=datetime.now(),
                                 frequency=level.label).dropna()
            df_final = pd.concat([df_cached, df_query.loc[last_idx:].iloc[1:]])
        else:
            df_final = df_cached
    else:
        if mod == local_mod:
            raise BaseException("mod is local but not found in cache " + contract)
        start_date = get_all_securities(['futures']).loc[contract]['start_date']
        df_final = get_price(contract, start_date=start_date,
                             end_date=datetime.now(), frequency=level.label, skip_paused=True, fill_paused=False)
    df_final.to_csv(cache_file_path)
    return format_quotes(df_final), df_final


def get_cache_path(contract: str, level: QuoteLevel):
    return cache_root_path + "/" + contract + ":" + level.label


def get_main_quote(type, end_time: datetime, count: int, level: QuoteLevel):
    contract = get_dominant_future(type)
    return get_quotes(contract, end_time, count, level)


def pull_quote():
    # 获取所有历史合约
    futures = list(get_all_securities(['futures']).index)
    continues = [x for x in futures if x.find('8888') >= 0]
    cached_files = os.listdir(cache_root_path)
    cached_contracts = [x.split('.')[0] for x in cached_files]
    for contract in continues:
        simple_name = contract.split('.')[0]
        if simple_name not in cached_contracts:
            get_all_quotes(contract, ONE_MINUTE)
            print("get quotes on " + contract)
            return
    print("all continues cached!")


if __name__ == '__main__':
    pull_quote()
